#include "FreeUniverse.h"

#include <Ogre.h>
#include <sstream>
#include <cmath>

#include "universe/data_structure.h"
#include "3rd_party/DynamicLines.h"

// Helper function for mouse events
CEGUI::MouseButton convertButton(OIS::MouseButtonID id)
{
    switch (id)
    {
    case OIS::MB_Left:
        return CEGUI::LeftButton;
    case OIS::MB_Right:
        return CEGUI::RightButton;
    case OIS::MB_Middle:
        return CEGUI::MiddleButton;
    default:
        return CEGUI::LeftButton;
    }
}

FreeUniverse::FreeUniverse()
    : mbShutDown(false),
      mbPause(false),
      mRotate(.13),
      mMove(25000),
      mDirection(Ogre::Vector3::ZERO),
      mbFollow(false),
      mFollowNode(NULL)
{
}

FreeUniverse::~FreeUniverse()
{
    //Remove ourself as a Window listener
    Ogre::WindowEventUtilities::removeWindowEventListener(mWindow, this);
    windowClosed(mWindow);
}

void FreeUniverse::setTime(const std::string& t)
{
    mTimeLabel->setText(t);
}

void FreeUniverse::setPlanets(set<string> planets)
{
    using namespace CEGUI;
    WindowManager& wmgr = CEGUI::WindowManager::getSingleton();
    PushButton* text;
    int i = 0;
    for (string planet : planets) {
        text = static_cast<PushButton*>(wmgr.createWindow("TaharezLook/Button"));
        text->setText(planet);
        text->setPosition(UVector2(UDim(0, 0), UDim(0, 30*(i++))));
        text->setSize(USize(UDim(1.0, 0), UDim(0, 30)));
        text->subscribeEvent(PushButton::EventClicked, CEGUI::Event::Subscriber(&FreeUniverse::onPlanetSelected, this));
        mPlanetPanel->addChild(text);
    }
}

bool FreeUniverse::paused() const
{
    return mbPause;
}

bool FreeUniverse::frameStarted(const Ogre::FrameEvent &event)
{
    if (!ApplicationBase::frameStarted(event))
        return false;

    if (!paused()) {
        if (mUSimulator.injectTimePulse(event.timeSinceLastFrame)) {
            mUniverse = mUSimulator.universe();
            updateUniverse();
        }
    }

    return true;
}

bool FreeUniverse::frameRenderingQueued(const Ogre::FrameEvent &event)
{
    if (!ApplicationBase::frameRenderingQueued(event))
        return false;

    mKeyboard->capture();
    mMouse->capture();

    if (mbShutDown)
        return false;

    static float d = 0;

    if (mDirection.isZeroLength())
        d = 0;
    else
        d = d + event.timeSinceLastFrame;

    mCamNode->translate(mDirection * event.timeSinceLastFrame * std::sqrt(d), Ogre::Node::TS_LOCAL);

    stepChangingFollow(event.timeSinceLastFrame);

    return true;
}

void FreeUniverse::setupListeners()
{
    ApplicationBase::setupListeners();

    if (!setupOIS(mWindow))
        throw std::runtime_error("初始化OIS失敗");

    //Set initial mouse clipping size
    windowResized(mWindow);
    //Register as a Window listener
    Ogre::WindowEventUtilities::addWindowEventListener(mWindow, this);
}

void FreeUniverse::createScene()
{
    ApplicationBase::createScene();

    mCamCentre = mSceneMgr->getRootSceneNode()->createChildSceneNode("CameraCentre");
    mCamNode->getParentSceneNode()->removeChild(mCamNode);
    mCamCentre->addChild(mCamNode);

    if (!setupCEGUI())
        throw std::runtime_error("初始化CEGUI失敗");

    mUSimulator.setTime(startTime);
    setTime(mUSimulator.getTimeStr());

    prepareToChangeFollow();

    for (string planet : mUSimulator.planets())
        createPlanet(planet);
    setPlanets(mPlanets);
    mUniverse = mUSimulator.universe();
    updateUniverse();

    Ogre::Light* sunLight = mSceneMgr->createLight("SunLight");
    sunLight->setDiffuseColour(1.0, 1.0, 0.8);
    sunLight->setSpecularColour(1.0, 1.0, 1.0);
    sunLight->setType(Ogre::Light::LT_POINT);
    sunLight->setAttenuation(10000000, 0.7, 0.00001, 0);
    dynamic_cast<Ogre::SceneNode*>(mSceneMgr->getRootSceneNode()->getChild("Solar"))->attachObject(sunLight);

    mSceneMgr->setSkyBox(true, "Background/SpaceSkyBox");
}

//Adjust mouse clipping area
void FreeUniverse::windowResized(Ogre::RenderWindow *window)
{
    unsigned int width, height, depth;
    int left, top;
    window->getMetrics(width, height, depth, left, top);
    const OIS::MouseState &ms = mMouse->getMouseState();
    ms.width = width;
    ms.height = height;
}

void FreeUniverse::windowClosed(Ogre::RenderWindow *window)
{
    //Only close for window that created OIS (the main window in these demos)
    if (window == mWindow) {
        unattachOIS();
    }
}

bool FreeUniverse::keyPressed(const OIS::KeyEvent &event)
{
    bool walk = false;
    switch (event.key) {
    case OIS::KC_ESCAPE:
        mbShutDown = true;
        break;
    case OIS::KC_UP:
    case OIS::KC_W:
        walk = true;
        mDirection.z = -mMove;
        break;
    case OIS::KC_DOWN:
    case OIS::KC_S:
        walk = true;
        mDirection.z = mMove;
        break;
    case OIS::KC_LEFT:
    case OIS::KC_A:
        walk = true;
        mDirection.x = -mMove;
        break;
    case OIS::KC_RIGHT:
    case OIS::KC_D:
        walk = true;
        mDirection.x = mMove;
        break;
    case OIS::KC_PGDOWN:
    case OIS::KC_E:
        walk = true;
        mDirection.y = -mMove;
        break;
    case OIS::KC_PGUP:
    case OIS::KC_Q:
        walk = true;
        mDirection.y = mMove;
        break;
    case OIS::KC_SPACE:
        mbPause = !mbPause;
        break;
    default:
        break;
    }
    if (mbPause && walk) {
        prepareToChangeFollow();
    }

    return true;
}

bool FreeUniverse::keyReleased(const OIS::KeyEvent &event)
{
    switch (event.key) {
    case OIS::KC_UP:
    case OIS::KC_W:
        mDirection.z = 0;
        break;
    case OIS::KC_DOWN:
    case OIS::KC_S:
        mDirection.z = 0;
        break;
    case OIS::KC_LEFT:
    case OIS::KC_A:
        mDirection.x = 0;
        break;
    case OIS::KC_RIGHT:
    case OIS::KC_D:
        mDirection.x = 0;
        break;
    case OIS::KC_PGDOWN:
    case OIS::KC_E:
        mDirection.y = 0;
        break;
    case OIS::KC_PGUP:
    case OIS::KC_Q:
        mDirection.y = 0;
        break;
    default:
        break;
    }

    return true;
}

bool FreeUniverse::mouseMoved(const OIS::MouseEvent &event)
{
    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectMousePosition(event.state.X.abs, event.state.Y.abs);

    if (event.state.buttonDown(OIS::MB_Middle)) {
        mCamNode->yaw(Ogre::Degree(-mRotate * event.state.X.rel), Ogre::Node::TS_WORLD);
        mCamNode->pitch(Ogre::Degree(-mRotate * event.state.Y.rel), Ogre::Node::TS_LOCAL);
    }
    if (event.state.buttonDown(OIS::MB_Right)) {
        mCamCentre->yaw(Ogre::Degree(-mRotate * event.state.X.rel), Ogre::Node::TS_LOCAL);
        mCamCentre->pitch(Ogre::Degree(-mRotate * event.state.Y.rel), Ogre::Node::TS_LOCAL);
    }

    return true;
}

bool FreeUniverse::mousePressed(const OIS::MouseEvent &event, OIS::MouseButtonID id)
{
    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectMouseButtonDown(convertButton(id));

    if (id == OIS::MB_Right) {
        CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().hide();
    }

    if (id == OIS::MB_Left) {
        CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
        CEGUI::Vector2f mousePos = context.getMouseCursor().getPosition();
        Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(
                    mousePos.d_x / float(event.state.width),
                    mousePos.d_y / float(event.state.height));

        Ogre::RaySceneQuery* rayScnQuery = mSceneMgr->createRayQuery(mouseRay);
        rayScnQuery->setSortByDistance(true);
        rayScnQuery->setQueryMask(PLANET_MASK);

        Ogre::RaySceneQueryResult& result = rayScnQuery->execute();

        bool found = false;

        for (Ogre::RaySceneQueryResult::iterator it = result.begin(); it != result.end(); it++) {
            found = it->movable &&
                    it->movable->getName() != "" &&
                    it->movable->getName() != "MainCamera" &&
                    it->movable->getName() != "CameraNode";
            if (found) {
                Ogre::SceneNode* node = it->movable->getParentSceneNode();
                prepareToChangeFollow(node);
                break;
            }
        }
    }

    return true;
}

bool FreeUniverse::mouseReleased(const OIS::MouseEvent &event, OIS::MouseButtonID id)
{
    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectMouseButtonUp(convertButton(id));

    if (id == OIS::MB_Right) {
        CEGUI::System::getSingleton().getDefaultGUIContext().getMouseCursor().show();
    }

    return true;
}

bool FreeUniverse::setupOIS(Ogre::RenderWindow* window)
{
    // Initialising OIS
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing OIS ***");
    OIS::ParamList pl;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    window->getCustomAttribute("WINDOW", &windowHnd);
    windowHndStr << windowHnd;
    pl.insert(std::make_pair(std::string("WINDOW"), windowHndStr.str()));
    pl.insert(std::make_pair(std::string("x11_mouse_grab"),std::string("false")));
    pl.insert(std::make_pair(std::string("x11_keyboard_grab"),std::string("false")));

    mInputManager = OIS::InputManager::createInputSystem(pl);

    mKeyboard = static_cast<OIS::Keyboard*>(mInputManager->createInputObject(OIS::OISKeyboard, false));
    mMouse = static_cast<OIS::Mouse*>(mInputManager->createInputObject(OIS::OISMouse, false));

    mMouse->setBuffered(true);
    mKeyboard->setBuffered(true);

    mMouse->setEventCallback(this);
    mKeyboard->setEventCallback(this);
    return true;
}

//Unattach OIS before window shutdown (very important under Linux)
bool FreeUniverse::unattachOIS()
{
    if(mInputManager) {
        mInputManager->destroyInputObject( mMouse );
        mInputManager->destroyInputObject( mKeyboard );

        OIS::InputManager::destroyInputSystem(mInputManager);
        mInputManager = 0;
    }

    return true;
}

bool FreeUniverse::setupCEGUI()
{
    Ogre::LogManager::getSingletonPtr()->logMessage("*** Initializing CEGUI ***");

    mRenderer = &CEGUI::OgreRenderer::bootstrapSystem();

    CEGUI::ImageManager::setImagesetDefaultResourceGroup("Imagesets");
    CEGUI::Font::setDefaultResourceGroup("Fonts");
    CEGUI::Scheme::setDefaultResourceGroup("Schemes");
    CEGUI::WidgetLookManager::setDefaultResourceGroup("LookNFeel");
    CEGUI::WindowManager::setDefaultResourceGroup("Layouts");

    CEGUI::SchemeManager::getSingleton().createFromFile("TaharezLook.scheme");
    CEGUI::FontManager::getSingleton().createFromFile("DejaVuSans-10.font");

    CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();

    context.setDefaultFont("DejaVuSans-10");
    context.getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");

    Ogre::LogManager::getSingletonPtr()->logMessage("Finished");


    CEGUI::WindowManager& wmgr = CEGUI::WindowManager::getSingleton();
    mRootWin = wmgr.loadLayoutFromFile("main.layout");

    CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(mRootWin);

    mTimeLabel = mRootWin->getChild("TimeLabel");
    mPlanetLabel = mRootWin->getChild("PlanetLabel");
    mPlanetPanel = mRootWin->getChild("PlanetPanel");

    mSpeedControl = dynamic_cast<CEGUI::Spinner*>(mRootWin->getChild("SpeedControl"));
    mSpeedControl->setCurrentValue(mUSimulator.speed());
    mSpeedControl->subscribeEvent(CEGUI::Spinner::EventValueChanged, CEGUI::Event::Subscriber(&FreeUniverse::onAlterSpeed, this));

    mDetailPanel = mRootWin->getChild("Detail");

    return true;
}

void FreeUniverse::setDetailVisible(bool setting)
{
    mDetailPanel->setVisible(setting);
}

void FreeUniverse::fillDetail(const PlanetInfo& info)
{
    mDetailPanel->setText(info.name);
    CEGUI::Window* w;
    std::stringstream ss;

    w = mDetailPanel->getChild("Radius");
    ss.clear();
    ss << info.radius;
    w->setText(ss.str());

    w = mDetailPanel->getChild("Mass");
    ss.clear();
    ss << info.mass;
    w->setText(ss.str());
}

bool FreeUniverse::onPlanetSelected(const CEGUI::EventArgs &e)
{
    const CEGUI::WindowEventArgs& we = dynamic_cast<const CEGUI::WindowEventArgs&>(e);
    const string pname = we.window->getText().c_str();
    Ogre::SceneNode* node = dynamic_cast<Ogre::SceneNode*>(mSceneMgr->getRootSceneNode()->getChild(pname));
    prepareToChangeFollow(node);
    return true;
}

bool FreeUniverse::onAlterSpeed(const CEGUI::EventArgs &e)
{
    int speed = mSpeedControl->getCurrentValue();
    mUSimulator.setSpeed(speed);
    return true;
}

Ogre::SceneNode* FreeUniverse::createPlanet(const string &name)
{
    string entityName = name + ".mesh";
    if (!Ogre::ResourceGroupManager::getSingleton().resourceExists("General", entityName)) {
        entityName = name + ".mesh";
        createSphere(entityName, mUSimulator.info(name), 64, 64);
    }
    Ogre::Entity* entity = mSceneMgr->createEntity(entityName);
    string materialName = "Planet/" + name;
    if (!Ogre::MaterialManager::getSingleton().resourceExists(materialName))
        materialName = "Planet/Dummy";
    entity->setMaterialName(materialName);
    entity->setQueryFlags(PLANET_MASK);
    Ogre::SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode(name);
    node->attachObject(entity);
    mPlanets.insert(name);

    DynamicLines *curve = new DynamicLines(Ogre::RenderOperation::OT_POINT_LIST);
    curve->setQueryFlags(CURVE_MASK);
    Ogre::SceneNode *cNode = mSceneMgr->getRootSceneNode()->createChildSceneNode(name + "_curve");
    cNode->attachObject(curve);
    cNode->setVisible(false);
    mPCurves[node] = cNode;

    return node;
}

void FreeUniverse::updatePlanet(const string &planetName)
{
    if (mUpdated[planetName])
        return;
    mUpdated[planetName] = true;
    Ogre::SceneNode* node;
    if (mPlanets.find(planetName) == mPlanets.end()) {
        node = createPlanet(planetName);
    } else {
        node = dynamic_cast<Ogre::SceneNode*>(mSceneMgr->getRootSceneNode()->getChild(planetName));
    }

    const string& centerName = mUSimulator.info(planetName).center;
    if (mFollowNode == NULL)
        updatePlanet(centerName);
    else if (mFollowNode != node)
        updatePlanet(mFollowNode->getName().c_str());

    const coordinate& coord = mUniverse.planets[mMapping[planetName]].coord;
    node->setPosition(coord.x, coord.y, coord.z);
    node->setVisible(true);

    if (mFollowNode == node || (!mFollowNode && centerName == planetName))
        return; // 沒必要更新路徑點，並非不可
    Ogre::SceneNode *cNode = mPCurves[node];
    DynamicLines *curve = dynamic_cast<DynamicLines*>(cNode->getAttachedObject(0));
    Ogre::Vector3 point(coord.x, coord.y, coord.z);
    if (mFollowNode)
        point = point - mFollowNode->getPosition();
    else
        point = point - mSceneMgr->getRootSceneNode()->getChild(centerName)->getPosition();
    curve->addPoint(point);
    curve->update();
    cNode->setVisible(true);
}

void FreeUniverse::updateUniverse()
{
    setTime(mUniverse.time);
    Ogre::SceneNode* rootNode = mSceneMgr->getRootSceneNode();
    for (auto name : mPlanets) {
        dynamic_cast<Ogre::SceneNode*>(rootNode->getChild(name))->setVisible(false);
    }

    for (size_t i = 0; i < mUniverse.planets.size(); i++) {
        const string &name = mUniverse.planets[i].name;
        mUpdated[name] = false;
        mMapping[name] = i;
    }
    for (size_t i = 0; i < mUniverse.planets.size(); i++) {
        updatePlanet(mUniverse.planets[i].name);
    }
}

void FreeUniverse::prepareToChangeFollow(Ogre::SceneNode *target)
{
    if (mFollowNode == target) {
        if (mbFollow)
            mCamNode->setPosition(0, 0, 4000);
        mbFollow = false;
    }
    if (target == NULL) {
        mPlanetLabel->setText("\\[Observing\\]");
        setDetailVisible(false);
    } else {
        mPlanetLabel->setText(target->getName());
        setDetailVisible(true);
        fillDetail(mUSimulator.info(target->getName()));
    }
    if (mbFollow) {
        mFollowNode->removeChild(mCamCentre);
        mSceneMgr->getRootSceneNode()->addChild(mCamCentre);
        mCamCentre->setPosition(mFollowNode->getPosition());
    }
    mbFollow = false;
    mFollowNode = target;
}

void FreeUniverse::stepChangingFollow(float deltaTime)
{
    static int count = 1;
    using namespace Ogre;
    auto finalize = [=](){
        mCamCentre->getParentSceneNode()->removeChild(mCamCentre);
        mFollowNode->addChild(mCamCentre);
        mCamCentre->setPosition(0,0,0);
        mbFollow = true;
        count = 1;

        SceneNode *pNode, *cNode, *centerNode;
        for (string planetName : mPlanets) {
            pNode = dynamic_cast<SceneNode*>(mSceneMgr->getRootSceneNode()->getChild(planetName));
            cNode = mPCurves[pNode];
            DynamicLines *curve = dynamic_cast<DynamicLines*>(cNode->getAttachedObject(0));
            curve->clear();
            curve->update();
            cNode->getParentSceneNode()->removeChild(cNode);
            if (mFollowNode) {
                centerNode = mFollowNode;
            } else {
                const string &centerName = mUSimulator.info(planetName).center;
                if (centerName != planetName)
                    centerNode = dynamic_cast<SceneNode*>(mSceneMgr->getRootSceneNode()->getChild(centerName));
                else
                    centerNode = mSceneMgr->getRootSceneNode();
            }
            centerNode->addChild(cNode);
        }
    };
    if (mbFollow || (mbFollow == false && mFollowNode == NULL))
        return;
    Vector3 am = mFollowNode->getPosition() - mCamCentre->getPosition();
    if (am.isZeroLength()) {
        finalize();
        return;
    }
    Vector3 delta = am / am.length() * 50000 * deltaTime * std::sqrt(count++);
    Vector3 newPos = mCamCentre->getPosition() + delta;
    Vector3 bm = mFollowNode->getPosition() - newPos;
    if (am.x*bm.x<0.1 && am.y*bm.y<0.1 && am.z*bm.z<0.1) {
        finalize();
    } else {
        mCamCentre->setPosition(newPos);
    }
}

void FreeUniverse::createSphere(const string &meshName, const PlanetInfo &info, const int nRings, const int nSegments)
{
    float r = info.radius / 300;
    using namespace Ogre;
    ManualObject * manual = mSceneMgr->createManualObject(meshName + ".manualObj");
    manual->begin("BaseWhiteNoLighting", RenderOperation::OT_TRIANGLE_LIST);
    float fDeltaRingAngle = (Math::PI / nRings);
    float fDeltaSegAngle = (2 * Math::PI / nSegments);
    unsigned short wVerticeIndex = 0 ;
    // Generate the group of rings for the sphere
    for (int ring = 0; ring <= nRings; ring++) {
        float r0 = r * sinf (ring * fDeltaRingAngle);
        float y0 = r * cosf (ring * fDeltaRingAngle);
        // Generate the group of segments for the current ring
        for (int seg = 0; seg <= nSegments; seg++) {
            float x0 = r0 * sinf(seg * fDeltaSegAngle);
            float z0 = r0 * cosf(seg * fDeltaSegAngle);
            // Add one vertex to the strip which makes up the sphere
            manual->position( x0, y0, z0);
            manual->normal(Vector3(x0, y0, z0).normalisedCopy());
            manual->textureCoord((float) seg / (float) nSegments, (float) ring / (float) nRings);
            if (ring != nRings) {
                // each vertex (except the last) has six indicies pointing to it
                manual->index(wVerticeIndex + nSegments + 1);
                manual->index(wVerticeIndex);
                manual->index(wVerticeIndex + nSegments);
                manual->index(wVerticeIndex + nSegments + 1);
                manual->index(wVerticeIndex + 1);
                manual->index(wVerticeIndex);
                wVerticeIndex ++;
            }
        }; // end for seg
    } // end for ring
    manual->end();

    MeshPtr mesh = manual->convertToMesh(meshName);
    mesh->_setBounds(AxisAlignedBox(Vector3(-r, -r, -r), Vector3(r, r, r)), false);
    mesh->_setBoundingSphereRadius(r);
    unsigned short src, dest;
    if (!mesh->suggestTangentVectorBuildParams(VES_TANGENT, src, dest)) {
        mesh->buildTangentVectors(VES_TANGENT, src, dest);
    }
}

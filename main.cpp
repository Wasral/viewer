#include <iostream>

#include "FreeUniverse.h"

using namespace std;

int main(int argc, char *argv[])
{
    // Create application object
    FreeUniverse app;
    try {
        app.go();
    } catch( Ogre::Exception& e ) {
        std::cerr << "An exception has occured: " <<
            e.getFullDescription().c_str() << std::endl;
    }

    return 0;
}

#ifndef DATABRIDGE_H
#define DATABRIDGE_H

#include <set>
#include <string>

#include "data_structure.h"

using std::set;
using std::string;

class DataBridge
{
public:
    virtual set<PlanetInfo> getAvailablePlanets() = 0;
    virtual set<string> getPlanetList() = 0;
    virtual Universe fetchUniverse(string time, set<string> planet) = 0;
protected:
    static set<string> parsePlanetList(string data);
    static Universe parseUniverse(string time, string data);
    static PlanetInfo parsePlanet(string data);
    static set<PlanetInfo> parseAllPlanets(string data);
    string waiter_ip;
    int waiter_port;
};

namespace DataBridgeFactory {
    DataBridge* create(string protocal, const string waiter_ip = "127.0.0.1", const int waiter_port = 9999);
}

#endif // DATABRIDGE_H

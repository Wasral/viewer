#include "databridge_rest.h"
#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

DataBridge_REST::DataBridge_REST(const string waiter_ip, const int waiter_port)
{
    this->waiter_ip =  waiter_ip;
    this->waiter_port = waiter_port;
    getPlanetList();
    for (string name : mPlanetList) {
        mPlanetInfos.insert(getPlanetInfo(name));
    }
}

set<string> DataBridge_REST::getPlanetList()
{
    static string data;
    if (mPlanetList.size() == 0) {
        data = sendGETRequest("/planet_list");
        mPlanetList = parsePlanetList(data);
    }
    return mPlanetList;
}

PlanetInfo DataBridge_REST::getPlanetInfo(string name)
{
    static string data;
    data = sendGETRequest("/info/"+name);
    PlanetInfo info = parsePlanet(data);
    return info;
}

set<PlanetInfo> DataBridge_REST::getAvailablePlanets()
{
    return mPlanetInfos;
}

Universe DataBridge_REST::fetchUniverse(string time, set<string> planet)
{
    string data = sendGETRequest("/universe/"+ time);
    return parseUniverse(time, data);
}

string DataBridge_REST::sendGETRequest(string url)
{
    tcp::endpoint endpoint(boost::asio::ip::address_v4::from_string(waiter_ip), waiter_port);
    tcp::socket socket(io_service);
    socket.connect(endpoint);

    boost::asio::streambuf request;
    std::ostream request_stream(&request);
    request_stream << "GET " << url << " HTTP/1.1\r\n";
    request_stream << "Host: " << string(waiter_ip) << "\r\n";
    request_stream << "Accept: text/*\r\n";
    request_stream << "Connection: close\r\n\r\n";
    boost::asio::write(socket, request);

    boost::asio::streambuf response;
    boost::asio::read_until(socket, response, "\r\n");

    std::istream response_stream(&response);
    std::string http_version;
    response_stream >> http_version;
    unsigned int status_code;
    response_stream >> status_code;
    std::string status_message;
    std::getline(response_stream, status_message);
    if (!response_stream || http_version.substr(0, 5) != "HTTP/")
    {
        std::cout << "無效響應\n";
        return "";
    }
    if (status_code != 200)
    {
        std::cout << "響應的狀態碼：" << status_code << "\n";
        return "";
    }

    size_t lheader = boost::asio::read_until(socket, response, "\r\n\r\n");
    response.consume(lheader);

    boost::system::error_code error;
    std::stringstream ss;
    ss << &response;

    while (boost::asio::read(socket, response, boost::asio::transfer_at_least(1), error))
        ss << &response;
    if (error != boost::asio::error::eof)
        throw boost::system::system_error(error);

    return ss.str();
}

#ifndef DATABRIDGE_TCP_H
#define DATABRIDGE_TCP_H

#include "databridge.h"
#include "data_structure.h"
#include <boost/asio.hpp>
#include <boost/property_tree/ptree.hpp>
#include <vector>

using std::pair;
using std::vector;
using boost::asio::ip::tcp;
using boost::property_tree::ptree;

class DataBridge_TCP
        : public DataBridge
{
public:
    DataBridge_TCP(const string waiter_ip, const int waiter_port);
    set<string> getPlanetList();
    set<PlanetInfo> getAvailablePlanets();
    Universe fetchUniverse(string time, set<string> planet);
protected:
    static string readData(tcp::socket& socket);
    static pair<string, ptree> parseResponse(string data, string expected = "");
    static Universe parseUniverseResponse(string data);
    static set<PlanetInfo> parseConsultResponse(string data);
    static string mkRequest(string type, ptree pt);
    static string mkUniverseRequest(string time, set<string> planet);
    static string mkConsultRequest(set<string> target);
    boost::asio::io_service ioservice;
    std::vector<tcp::socket> sockets;
    set<PlanetInfo> mPlanetInfos;
};

#endif // DATABRIDGE_TCP_H

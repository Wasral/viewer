#include "ucluster.h"

ucluster::ucluster()
{
}

size_t ucluster::size()
{
    return universes.size();
}

bool ucluster::insert(date time, Universe universe)
{
    std::lock_guard<std::mutex> lck(mt_u);
    universes.insert(dumap::value_type(time, universe));
    return true;
}

bool ucluster::free(date time)
{
    std::lock_guard<std::mutex> lck(mt_u);
    universes.erase(time);
    for (dumap::const_iterator it = universes.cbegin(); it != universes.cend() && it->first <= time; it++) {
        universes.erase(it);
    }
    return true;
}

const Universe &ucluster::operator[](date time)
{
    dumap::iterator it;
    while (true) {
        std::lock_guard<std::mutex> lck(mt_u);
        it = universes.find(time);
        if (it != universes.end())
            return it->second;
    }
}

const Universe &ucluster::operator[](size_t index)
{
    while (true) {
        std::lock_guard<std::mutex> lck(mt_u);
        if (universes.size() > index) {
            dumap::const_iterator it = universes.cbegin();
            for (int i = 0; i < index; i++)
                it++;
            return it->second;
        }
    }
}

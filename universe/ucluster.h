#ifndef UCLUSTER_H
#define UCLUSTER_H

#include <mutex>
#include <map>
#include "data_structure.h"
#include "date.h"

using std::map;

typedef map<date, Universe> dumap;

class ucluster
{
public:
    ucluster();
    size_t size();
    bool insert(date time, Universe universe);
    bool free(date time);
    const Universe& operator[](date time);
    const Universe& operator[](size_t index);
protected:
    dumap universes;
    std::mutex mt_u;
};

#endif // UCLUSTER_H
